
import { Injectable } from '@angular/core';
import { Platform, Config } from 'ionic-angular';
// import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';

import { App } from 'ionic-angular';


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UtilService } from './util.service';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/of';

import * as jwt from 'jwt-decode';
import * as jwtClaims from 'jwt-claims';

/** NOSSOS PACOTES */
import { AnalyticsService } from '@ons/ons-mobile-analytics';

/** PAGES */
import { LoginPage } from '../Pages/login/login';

/** SERVICES */
import { TokenService } from './token.service';
import { ErrorService } from './error.service';
import { UserService } from './user.service';
import { StorageService } from './storage.service';
import { EnvironmentService } from './environment.service';
import { NetWorkService } from './network.service';

/** INTERFACES */
import * as User from './../interfaces/user.model';
import * as NetState from './../interfaces/network.model';
import * as Parameter from './../interfaces/parametros.model';

/** CONFIG */
import { Config as config } from './../environment/config';



/**
 * Esta classe é o coração do componente.
 * Nela são processadas as autenticações e validações de usuário. Todos os serviços são consumidos a partir
 * deste ponto.
 * 
 * O evento onConectedChange é setado aqui sempre que o usuário é autenticado, passando o objeto usuário ativo.
 * 
 * 
 */
@Injectable()
export class LoginService {

  public onConectedChange = new Subject<User.User>();

  private _parameters: Parameter.Parameters = <Parameter.Parameters>{ aplication_name: '' };
  private _currentUser: User.User;
  public VoltandoSaida = false;
  public Usuario_alterado = false;
  public press_exit = false;


  /**
   * Retorna o objeto do usuário em curso
   */
  get CurrentUser(): User.User {
    return this.userSrv.User;
  }

  // get VoltandoSaida() {
  //   this.storageSrv.recuperar(config.Keys.autoLogin)
  //     .then((resultado) => {
  //       return resultado;
  //     });
  // }

  constructor(public utilSrv: UtilService,
    public tokenSrv: TokenService,
    public http: HttpClient,
    public userSrv: UserService,
    public analyticsSrv: AnalyticsService,
    public netSrv: NetWorkService,
    public errorSrv: ErrorService,
    public storageSrv: StorageService,
    public app: App,
    public fingerAuth: FingerprintAIO,
    public configSrv: Config,
    public platform: Platform,
    public envSrv: EnvironmentService,
    public alertCtrl: AlertController
  ) {
    this._currentUser = userSrv.empty();
  }

  /**
   * Seta o nome da aplicação a ser utilizado na autenticação com o POP
   * @param app Nome da aplicação
   */
  public setAplicationName(app: string) {
    this._parameters.aplication_name = app;
  }
  /**
   * Executa a autenticação do usuário
   * @param user Usuário
   * @param pass Senha
   */
  public login(user: string, pass: string): Observable<any> {
    if (this._parameters.aplication_name === '') {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO, '');
      this.errorSrv.add(config.Error.PARAMETER_ERROR, config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO);
      throw new Error(config.mensagemGenerica.ERRO_FALTA_NOME_APLICACAO);
    } else {
      this.validarUsuario(user, pass)
        .subscribe(result => {
          switch (result.status) {
            case config.userValidate.USUARIO_OU_SENHA_INVALIDOS:
              this.analyticsSrv.sendCustomEvent(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, result);
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.LOGIN_DIGITAL_PADRAO:
              break;
            case config.userValidate.LOGIN_DIGITAL_SENHA_EM_BRANCO:
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.LOGIN_DIGITAL_SENHA_EM_BRANCO:
              this.utilSrv.alerta(result.mensagem);
              break;
            case config.userValidate.DADOS_VALIDOS:
              this.utilSrv.ativarBloqueioMsg(config.mensagemGenerica.ENTRANDO);
              this.autenticarPOP(user, pass)
                .subscribe(res => {
                  if (res) {
                    this.storageSrv.deletar(config.Keys.USUARIO_SAIU);
                    this.storageSrv.recuperar(config.Keys.FINGER_KEY)
                      .then((autorizacao) => {
                        if (!autorizacao || autorizacao === undefined || autorizacao === null) {
                          if (this.platform.is('cordova')) {
                            this.utilSrv.desativarBloqueioMsg();
                            this.fingerAuth.isAvailable().then((valor) => {
                              let alert = this.alertCtrl.create({
                                title: config.mensagemGenerica.AUTENTICAR_BIOMETRIA,
                                message: config.mensagemGenerica.AUTENTICAR_BIOMETRIA_CONFIRMAR,
                                buttons: [
                                  {
                                    text: 'Não',
                                    role: 'cancel',
                                    handler: () => {
                                      this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                                    }
                                  },
                                  {
                                    text: 'Sim',
                                    handler: () => {
                                      this.ConfirmaFinger().then((confirmado) => {

                                        if (confirmado) {
                                          this.storageSrv.Gravar(config.Keys.FINGER_KEY, true);
                                          this.utilSrv.
                                            alerta(config.mensagemGenerica.AUTENTICAR_BIOMETRIA_OK, 4000,
                                              'middle', 'Autenticação digital!');
                                        } else {
                                          this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                                          this.utilSrv.
                                            alerta(
                                              config.mensagemGenerica.AUTENTICAR_BIOMETRIA_ERRO, 4000, 'middle', 'Autenticação digital!');
                                        }
                                      });
                                    }
                                  }
                                ]
                              });
                              alert.present();
                            }).catch((err) => {
                              this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                              this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_UNSUPORTED, err);
                              this.errorSrv.add(config.Error.FINGER_ID_UNSUPORTED, err);
                              return false;
                            });
                          } else {
                            this.storageSrv.Gravar(config.Keys.FINGER_KEY, false);
                            this.utilSrv.desativarBloqueioMsg();
                          }
                        } else {
                          this.utilSrv.desativarBloqueioMsg();
                        }
                      });
                  } else {
                    if (!this.netSrv.isNetworkConnected().connected) {
                      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS, '');
                    } else {
                      this.utilSrv.alerta(config.mensagemGenerica.USUARIO_OU_SENHA_INVALIDOS);
                    }
                    this.utilSrv.desativarBloqueioMsg();
                  }
                  this.VoltandoSaida = false;
                  // this.utilSrv.desativarBloqueioMsg();
                }, (err) => {
                  let message: string;
                  if (err.status && (err.status === 401 || err.status === 400)) {
                    message = config.mensagemGenerica.USUARIO_NAO_AUTORIZADO + ' erro: ' + err;
                  } else {
                    this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.AUTHENTICATION_ERROR_POP, err);
                    this.errorSrv.add(config.Error.AUTHENTICATION_ERROR_POP, message);
                  }
                  this.utilSrv.desativarBloqueioMsg();
                  this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_VERIFICAR_SENHA, '');
                  this.utilSrv.alerta(config.mensagemGenerica.USUARIO_VERIFICAR_SENHA, 4000, 'top', 'Não foi possivel autenticar!');
                }); break;
          }
        });
      /**
       * Este retorno não é relevante nesta versão pois o tratamento é feito com o Subject 
       */
      return Observable.of(true);
    }
  }


  /**
   * Confirma se o usuário deseja utilizar ou não o finger autentication
   * Somente será exibido se o device possuir este recurso.
   */
  public ConfirmaFinger(): Promise<boolean> {
    return this.fingerAuth.isAvailable().then((valor) => {
      return this.fingerAuth.show({
        clientId: 'keychain.br.org.ons',
        clientSecret: 'password', // Only necessary for Android
        disableBackup: true,  // Only for Android(optional)
        localizedReason: 'Toque no sensor para entrar' // Only for iOS
      }).then((result: any) => {
        return true;
      })
        .catch((err: any) => {
          this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_ERROR, err);
          this.errorSrv.add(config.Error.FINGER_ID_ERROR, err);
          return false;
        });

    }).catch((err) => {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.FINGER_ID_UNSUPORTED, err);
      this.errorSrv.add(config.Error.FINGER_ID_UNSUPORTED, err);
      return false;
    });
  }

  /**
   * Faz a autenticação do usuário no POP
   * Os dados de usuário e senha NÃO SÃO ARMAZENADOS.
   * 
   * @param user Nome do usuário a ser autenticado
   * @param pass Senha do usuário a ser autenticado
   */
  public autenticarPOP(user: string, pass: string): Observable<boolean> {
    let connected: boolean;
    let values: any = {
      username: user
      , password: pass
      , client_id: this._parameters.aplication_name
      , grant_type: config.GRANT_TYPE
    };
    let result: boolean;
    if (this.netSrv.isNetworkConnected().connected) {
      const body = Object.keys(values)
        .map((key) => { return encodeURIComponent(key) + '=' + encodeURIComponent(values[key]); }).join('&');
      return this.http.post(this.envSrv.current.value + '/token', body, this.utilSrv.httpOptions)
        .map(
          data => {
            this.tokenSrv.setToken(data);
            this.storageSrv.Gravar(config.Keys.tokenUser, data);
            this.storageSrv.Gravar(config.Keys.autoLogin, config.VoltandoSaida.INICIALIZADO);
            this.onConectedChange.next(this.fillUser());
            result = true;
            return true;
          },
          (err) => {
            this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.AUTHENTICATION_ERROR_POP, err);
            this.utilSrv.desativarBloqueioMsg();
            this.utilSrv.ErrorAlerta('err', 2000, 'middle', 'Erro no Pop');
            this.errorSrv.add(config.Error.AUTHENTICATION_ERROR_POP, 'Erro de autenticação');
            result = false;
            return false;

          });
    } else {
      this.utilSrv.alerta(config.NetWorkMessages.SEM_CONEXAO);
      result = false;
      return Observable.of(false);
    }
  }

  /**
   * Faz o processo de validação do token com o qual o sistema executa o direcionamento automático para o
   * page destino.
   */
  public validateTokenFlow(): Promise<any> {
    return this.tokenSrv.getTokenFromStorage()
      .then(data => {
        return data;
      }).catch((err) => {
        this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.TOKEN_ERROR, err);
        //  this.onStorageError.next(err);
        return null;
      });
  }


  /**
   * Preenche o objeto usuário com os dados do token
   *
   */
  public fillUser(): User.User {
    let u: User.User = this.userSrv.empty();
    u.email = this.tokenSrv.getDecodedByName(config.Claim_Email);
    u.User_Full_Name = this.tokenSrv.getDecodedByName(config.Claim_UserFullName);
    u.User_Name = this.tokenSrv.getDecodedByName(config.Claim_Account);
    u.Connected_At = new Date();
    u.Connected = true;
    this.userSrv.setUser(u);
    this._currentUser = u;
    return u;
  }

  /**
   * Desconecta o usuário da sessão atual
   */
  public logout() {
    this.press_exit = true;
    this.VoltandoSaida = true;
    this.storageSrv.Gravar(config.Keys.USUARIO_SAIU, true);

    this.userSrv.setUser(this.userSrv.empty());
    this.app.getActiveNav().setRoot(LoginPage);
  }

  /**
   * Retira o usuário do app.
   * Todos os dados da execução são cancelados 
   * Os dados armazenados não são alterados.
   */
  public exitApp() {
    this.VoltandoSaida = false;
    this.storageSrv.limparBase();
    this.storageSrv.deletar(config.Keys.USUARIO_SAIU);
    this.userSrv.setUser(this.userSrv.empty());
    this.storageSrv.deletar(config.Keys.autoLogin); //, config.VoltandoSaida.VOLTANDO);
    this.app.getActiveNav().setRoot(LoginPage);
  }

  /**
   * Retorna o objeto com os parâmetros do componente
   */
  public getParameter(): Parameter.Parameters {
    return this._parameters;
  }

  /**
   * Faz a validação do usuário para autenticação
   * Este metodo não autentica o usuário, apenas faz a verificação dos dados básicos
   *
   * @param usuario Usuário a ser validado
   * @param senha Senha do usuário a ser validada
   * 
   */
  validarUsuario(usuario: string, senha: string): Observable<any> {
    let result: any = {};
    // result.mensagemArray = []
    result.status = 'VALIDO';
    result.mensagem = '';
    try {
      if (this.VoltandoSaida) {
        if (usuario && !senha) {
          if (this.Usuario_alterado) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_E_SENHA_ALTERADOS, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
            };
          } else {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
            }
          }
        } else {
          if (usuario && senha) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
            };
          } else {
            if (!usuario && !senha) {
              result = {
                'mensagem': config.mensagemGenerica.USUARIO_SENHA_NAOINFORMADOS, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
              };
            }
          }
        }
      } else {
        if (!usuario && !senha) {
          result = {
            'mensagem': config.mensagemGenerica.USUARIO_SENHA_NAOINFORMADOS,
            'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
          };
        } else {
          if (!usuario && senha) {
            result = {
              'mensagem': config.mensagemGenerica.USUARIO_NAOINFORMADO, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
            };
          } else {
            if (usuario && !senha) {
              result = {
                'mensagem': config.mensagemGenerica.USUARIO_SENHA_BRANCO, 'status': config.userValidate.USUARIO_OU_SENHA_INVALIDOS
              };
            } else {
              if (usuario && senha) {
                result = {
                  'mensagem': config.mensagemGenerica.USUARIO_SENHA_OK, 'status': config.userValidate.DADOS_VALIDOS
                };
              }
            }
          }
        }
      }
      return Observable.of(result);
    } catch (error) {
      this.analyticsSrv.sendNonFatalCrash(config.mensagemGenerica.USUARIO_ERRO_VALIDACAO, error);
    }
  }

}
