import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SecurityService } from '@ons/ons-mobile-login';

@IonicPage()
@Component({
  selector: 'page-permissoes',
  templateUrl: 'permissoes.html',
})
export class PermissoesPage {
  UseSecurity = true;
  ScopeTypeOperationAllowed = false;
  ScopeTypeGroupAllowed = false;
  ScopeTypeRoleAllowed = false;
  IsGlobal = false;

  inputScope = "ONS";
  inputType = "ONS";
  inputOperation = "";
  inputGroup = "";
  inputRole = "";


  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public securitySrv: SecurityService) {


  }

  globalChange(){
    this.init();
}

  scopeChange(){
    this.init();
  }

  typeChange(){
   this.init();
  }

  groupChange(){
    // if(!this.IsGlobal)
    // {
      this.ScopeTypeGroupAllowed = this.securitySrv.isGroupAllowedByScope(this.inputScope,this.inputType,this.inputGroup);
    // }
    // else
    // {
    //   this.ScopeTypeGroupAllowed = true;
    // }
  }

  roleChange(){
    // if(!this.IsGlobal)
    // {
      this.ScopeTypeRoleAllowed = this.securitySrv.isRoleAllowedByScope(this.inputScope,this.inputType,this.inputRole);
    // }
    // else
    // {
    //   this.ScopeTypeRoleAllowed = true;
    // }
  }

  operationChange(){
    if(!this.IsGlobal)
    {
      this.ScopeTypeOperationAllowed = this.securitySrv.isOperationAllowedByScope(this.inputScope,this.inputType,this.inputOperation);
    }
    else
    {
      this.ScopeTypeOperationAllowed = this.securitySrv.isGlobalAccessByScope(this.inputScope,this.inputOperation);
       //true;
    }
  }

  init()
  {
    this.groupChange();
    this.roleChange();
    this.operationChange();
  }

}
