import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SecurityService, Config } from '@ons/ons-mobile-login';

/**
 * Generated class for the PermissoesnoscopePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-permissoesnoscope',
  templateUrl: 'permissoesnoscope.html',
})
export class PermissoesnoscopePage {
  UseSecurity = true;
  OperationAllowed = false;
  GroupAllowed = false;
  RoleAllowed = false;
  IsGlobal = false;

  inputOperation = "";
  inputGroup = "";
  inputRole = "";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public securitySrv: SecurityService) {
  }

  globalChange(){
      this.groupChange();
      this.roleChange();
      this.operationChange();
  }

  groupChange(){
    // if(!this.IsGlobal)
    // {
      this.GroupAllowed = this.securitySrv.IsAllowed(Config.ClaimTags.Group,this.inputGroup);
    // }
    // else
    // {
    //   this.GroupAllowed = true;
    // }

  }

  roleChange(){
    // if(!this.IsGlobal)
    // {
      this.RoleAllowed = this.securitySrv.isRoleAllowed(this.inputRole);
    // }
    // else
    // {
    //   this.RoleAllowed = true;
    // }
  }

  operationChange(){
    if(!this.IsGlobal)
    {
      this.OperationAllowed = this.securitySrv.IsAllowed(Config.ClaimTags.Operation,this.inputOperation);
    }
    else
    {
      this.OperationAllowed = this.securitySrv.isGlobalAccess(this.inputOperation);// true;
    }
  }

}
