import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Platform } from 'ionic-angular';

import { LoginService, EnvironmentService, LoginPage, ImageService } from '@ons/ons-mobile-login';
import { AnalyticsService } from '@ons/ons-mobile-analytics';

import { PermissoesPage } from '../permissoes/permissoes';
import { PermissoesnoscopePage } from '../permissoesnoscope/permissoesnoscope';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController
    , public platform: Platform
    , public loginSrv: LoginService
    , public analyticSrv: AnalyticsService
    , public envSrv: EnvironmentService
    , public imageSrv: ImageService
  ) {
    loginSrv.setAplicationName('Mobile.Contatos')
    envSrv.setEnv('TST');
    platform.ready().then(() => {
      if (!loginSrv.CurrentUser.Connected) {
        imageSrv.SetDefault();
        navCtrl.setRoot(LoginPage)
      }
    });
  }

  ionViewDidLoad() {
    this.loginSrv.onConectedChange
      .subscribe((u: any) => {
        console.log(this.analyticSrv);
        if (u !== undefined) {
          if (u.Connected) {
            this.analyticSrv.sendCustomEvent("Logar");
            this.navCtrl.setRoot(HomePage)
          }
        }
      })
  }

  sair() {
    this.loginSrv.logout()
    //this.navCtrl.setRoot(LoginPage)
  }
  encerrar() {
    this.loginSrv.exitApp();
  }

  goHome() {
    this.navCtrl.setRoot(HomePage)
  }
  goToPermissoes(){
    this.navCtrl.push(PermissoesPage);
    }
  
    goToPermissoesSemEscopo(){
      this.navCtrl.push(PermissoesnoscopePage);
    }
    
  errorAdd() {

  }

}
