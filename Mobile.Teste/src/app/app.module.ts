import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PermissoesPage } from '../pages/permissoes/permissoes';
import { PermissoesnoscopePage } from '../pages/permissoesnoscope/permissoesnoscope';

import { OnsPackage, LoginService, ErrorService, LoginPage, UserService, StorageService, ImageService } from '@ons/ons-mobile-login'
import { AnalyticsService, OnsAnalyticsModule } from '@ons/ons-mobile-analytics';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PermissoesPage,
    PermissoesnoscopePage

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    OnsAnalyticsModule.forRoot(),
    OnsPackage.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    PermissoesPage,
    PermissoesnoscopePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LoginService,
    AnalyticsService,
    ErrorService,
    ImageService,
    UserService,
    StorageService,
    FingerprintAIO
  ]
})
export class AppModule { }
