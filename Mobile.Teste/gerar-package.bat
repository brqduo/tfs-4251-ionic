@echo off
rmdir node_modules /s /q
rmdir www /s /q
rmdir .source_maps /s /q
echo Copiando Imagens
md src\assets\login
copy node_modules\ons-mobile-login\assets\loginImages\*.* src\assets\login

CALL npm i  rxjs@5.5.11 --save
call npm i zone.js@^0.8.4 --save
call npm i @auth0/angular-jwt@1.2.0 -- save
call npm i jwt-claims@^1.0.1 --save
call npm i jwt-decode@^2.2.0 --save
call npm i ionic-angular@3.9.2 --save

call npm i ons-mobile-analytics@latest --registry http://10.2.1.127:4873
call npm i ons-mobile-login@latest --registry http://10.2.1.127:4873
call npm i