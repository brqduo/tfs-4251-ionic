import { AnalyticsService, OnsAnalyticsModule } from '@ons/ons-mobile-analytics';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IonicModule, NavController } from 'ionic-angular';

import { ErrorService } from './services/error.service';
import { TokenService } from './services/token.service';
import { LoginService } from './services/login.service';
import { EnvironmentService } from './services/environment.service';

import { UtilService } from './services/util.service';
import { StatusTableService } from './services/status-table.service';
import { UserService } from './services/user.service';
import { StorageService } from './services/storage.service';
import { LoginPage } from './Pages/login/login';


import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { IonicStorageModule } from '@ionic/storage';
import { ImageService } from './services/images.service';
import { SecurityService } from './services/security.service';
import { NetWorkService } from './services/network.service';

export * from './Pages/login/login';
export * from './services/token.service';
export * from './services/login.service';
export * from './services/status-table.service';
export * from './services/error.service';
export * from './services/user.service';
export * from './services/util.service';
export * from './services/images.service';
export * from './services/storage.service';
export * from './environment/config';
export * from './services/security.service';
export * from './services/network.service';
// export * from './environment/environment';
export * from './services/environment.service';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        IonicStorageModule.forRoot()
    ],
    declarations: [
        LoginPage
    ],
    exports: [
        LoginPage
    ]
})
export class OnsPackage {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: OnsPackage,
            providers: [
                LoginService,
                ErrorService,
                TokenService,
                UtilService,
                UserService,
                AnalyticsService,
                StatusTableService,
                StorageService,
                SecurityService,
                NetWorkService,
                ImageService,
                EnvironmentService,
                // Plugings ↓
                FingerprintAIO
            ]
        };
    }
}
