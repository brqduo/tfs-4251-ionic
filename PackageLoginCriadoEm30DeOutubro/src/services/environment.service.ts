import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

/** CONFIG */
import { Config as config } from './../environment/config';

/** NOSSOS PACOTES */
import { AnalyticsService } from '@ons/ons-mobile-analytics';


@Injectable()
export class EnvironmentService {

    private _currentEnv: any = { name: 'Production', value: config.SERVER_URL_PRD };
    get current() {
        return this._currentEnv;
    }
    constructor(public analyticSrv: AnalyticsService) {

    }

    /**
     * Retorna a lista de erros ocorridos na sessão em curso
     */
    public setEnv(env?: string) {
        switch (env) {
            case (null || undefined): {
                this._currentEnv = { name: 'Production', value: config.SERVER_URL_PRD };
                break;
            }
            case 'PRD': {
                this._currentEnv = { name: 'Production', value: config.SERVER_URL_PRD };
                break;
            }
            case 'TST': {
                this._currentEnv = { name: 'Test', value: config.SERVER_URL_TST };
                break;
            }
            case 'HOM': {
                this._currentEnv = { name: 'Homologação', value: config.SERVER_URL_HOM };
                break;
            }
            case 'DSV': {
                this._currentEnv = { name: 'Desenvolvimento', value: config.SERVER_URL_DEV };
                break;
            }
            default: {
                if (env.toLowerCase().startsWith('http')) {
                    this._currentEnv = { name: 'Customizado', value: env };
                } else {
                    this._currentEnv = { name: 'inválido', value: 'Parametro invalido para o ambiente. Validos DEV,TST,HOM, PRD' };
                }
                break;
            }
        }
        this.analyticSrv.sendCustomEvent('Configurar Ambiente', this._currentEnv);
        return this.current;
    }

}
