import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { PermissoesPage } from '../permissoes/permissoes';
import { PermissoesnoscopePage } from '../permissoesnoscope/permissoesnoscope';
import { LoginService, TokenService, StorageService, LoginPage, ImageService, SecurityService, Config } from '@ons/ons-mobile-login';
import jwt from 'jwt-decode';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tokenTest = {
    access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9hdXRob3JpemF0aW9uZGVjaXNpb24iOiJNb2JpbGUuTWV1T05TIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYXV0aGVudGljYXRpb24iOiJQT1AiLCJlbWFpbCI6ImFndWxhb0BvbnMub3JnLmJyIiwidW5pcXVlX25hbWUiOiJvbnNcXGFndWxhbyIsIm5hbWVpZCI6Im9uc1xcYWd1bGFvIiwid2luYWNjb3VudG5hbWUiOiJvbnNcXGFndWxhbyIsImdpdmVuX25hbWUiOiJBTkRFUlNPTiBSQU5HRUwgR1VMQU8iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDE1LzA3L2lkZW50aXR5L2NsYWltcy9zY29wZW9wZXJhdGlvbiI6WyJPTlMvT05TfCMkJUFwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlTGlzdGFyIFJlY2ViaW1lbnRvcyBGw61zaWNvcyIsIk9OUy9PTlN8IyQlUmVwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIGFycXVpdm8gYW5leG8gcmVjZWJpbWVudG8gZsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIHJlZ2lzdHJvcyBkYSBjYXRyYWNhIl0sImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3Njb3Blcm9sZSI6WyJPTlMvT05TfCMkJUFwcm92YWRvciBUw6ljbmljbyIsIk9OUy9PTlN8IyQlRnVuY2lvbsOhcmlvIEdlcmFsIiwiT05TL09OU3wjJCVHZXN0b3IiXSwicm9sZSI6WyJBcHJvdmFkb3IgVMOpY25pY28iLCJGdW5jaW9uw6FyaW8gR2VyYWwiLCJHZXN0b3IiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiUy0xLTUtMjEtNjQ0Mjk4OTg1LTE5NjI5Mjg2MC0xOTE2ODE1ODM2LTcxNzQwIiwicHJpbWFyeXNpZCI6IlMtMS01LTIxLTY0NDI5ODk4NS0xOTYyOTI4NjAtMTkxNjgxNTgzNi03MTc0MCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3VzZXJ0aWNrZXQiOiJCMTVCMjc5NkM4MzFCNkFBN0EwRTEzNDMyNjE1N0Q2REIwRDY1NDNGOTY2NDNEMjgxNjgwRkQ5MjBFQzU4NzQ4NENDMzZFNjk1NUE5OThCNTA4MTVDQjU0NjJDMzFEMTdCMDc0RUI1NjE1QUZCM0E0NDJFQkRGOTlFMkFCMTJDMkIyQjg4MjNDOUJGQzQ1Mzc2QTJFQTlFOUMwNUZGRTkzNEM3QURDQjA5M0I1QjcxNzQxMUM4QkM4RTFCOUExM0E4NzQ4NkJEQjA0QkU4MjAwQjkxM0YwMjkzNEJFMDBCQkNBQTQ5RTczIiwidXBuIjoiYWd1bGFvQG9ucy5vcmcuYnIiLCJpc3MiOiJodHRwOi8vcG9wZHN2Lm9ucy5vcmcuYnIvb25zLnBvcC5mZWRlcmF0aW9uLyIsImF1ZCI6Ik1vYmlsZS5NZXVPTlMiLCJleHAiOjE1MzU0NzM0OTgsIm5iZiI6MTUzNTQ3MzE5OH0.pF73BcZLAbHTGCImCIWVHHQ2O93jlov_5VDsAwKTqFY",
    expires_in: 1199,
    refresh_token: "fY9gNU9s8eXre/DeajBRyGCBKNgNzfPj4YRqlyq7QEs=",
    token_type: "bearer",
  }


  ClaimTags = {
    Group: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/group',
    ScopeRole: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/scoperole',
    UserTicket: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket',
    Scope: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/scope',
    Operation: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/operation'
  }
  constructor(public navCtrl: NavController
    , public storageSrv: StorageService
    , public platform: Platform
     , public securitySrv: SecurityService 
    , public imageSrv: ImageService
    , public loginSrv: LoginService
    , public viewCtrl: ViewController
    , public tokenSrv: TokenService) {
    loginSrv.setAplicationName('Mobile.Contatos')
    
    console.log('Home Page');

    var groups = this.securitySrv.Groups;
    console.log('Grupos: ');
    console.log(groups);

    //this.getDecode();
    console.log('Token: ', this.tokenSrv.getToken());
    
    platform.ready().then(() => {
      if (!loginSrv.CurrentUser.Connected) {
        imageSrv.SetDefault();
       

        //this.navCtrl.setRoot(HomePage)
        //navCtrl.setRoot(LoginPage)
      }
    });
    
    
    //this.start();
  }

  ionViewDidLoad() {

    this.loginSrv.onConectedChange
      .subscribe((u: any) => {
        if (u !== undefined) {
          if (u.Connected) {
            console.log('castle 2');
            this.navCtrl.setRoot(LoginPage)
          }
        }
      })
  }

  getDecode() {
    this.tokenSrv.setToken(this.tokenTest);
   // this.tokenSrv.setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9hdXRob3JpemF0aW9uZGVjaXNpb24iOiJNb2JpbGUuTWV1T05TIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYXV0aGVudGljYXRpb24iOiJQT1AiLCJlbWFpbCI6ImFndWxhb0BvbnMub3JnLmJyIiwidW5pcXVlX25hbWUiOiJvbnNcXGFndWxhbyIsIm5hbWVpZCI6Im9uc1xcYWd1bGFvIiwid2luYWNjb3VudG5hbWUiOiJvbnNcXGFndWxhbyIsImdpdmVuX25hbWUiOiJBTkRFUlNPTiBSQU5HRUwgR1VMQU8iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDE1LzA3L2lkZW50aXR5L2NsYWltcy9zY29wZW9wZXJhdGlvbiI6WyJPTlMvT05TfCMkJUFwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlTGlzdGFyIFJlY2ViaW1lbnRvcyBGw61zaWNvcyIsIk9OUy9PTlN8IyQlUmVwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIGFycXVpdm8gYW5leG8gcmVjZWJpbWVudG8gZsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIHJlZ2lzdHJvcyBkYSBjYXRyYWNhIl0sImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3Njb3Blcm9sZSI6WyJPTlMvT05TfCMkJUFwcm92YWRvciBUw6ljbmljbyIsIk9OUy9PTlN8IyQlRnVuY2lvbsOhcmlvIEdlcmFsIiwiT05TL09OU3wjJCVHZXN0b3IiXSwicm9sZSI6WyJBcHJvdmFkb3IgVMOpY25pY28iLCJGdW5jaW9uw6FyaW8gR2VyYWwiLCJHZXN0b3IiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiUy0xLTUtMjEtNjQ0Mjk4OTg1LTE5NjI5Mjg2MC0xOTE2ODE1ODM2LTcxNzQwIiwicHJpbWFyeXNpZCI6IlMtMS01LTIxLTY0NDI5ODk4NS0xOTYyOTI4NjAtMTkxNjgxNTgzNi03MTc0MCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3VzZXJ0aWNrZXQiOiJCMTVCMjc5NkM4MzFCNkFBN0EwRTEzNDMyNjE1N0Q2REIwRDY1NDNGOTY2NDNEMjgxNjgwRkQ5MjBFQzU4NzQ4NENDMzZFNjk1NUE5OThCNTA4MTVDQjU0NjJDMzFEMTdCMDc0RUI1NjE1QUZCM0E0NDJFQkRGOTlFMkFCMTJDMkIyQjg4MjNDOUJGQzQ1Mzc2QTJFQTlFOUMwNUZGRTkzNEM3QURDQjA5M0I1QjcxNzQxMUM4QkM4RTFCOUExM0E4NzQ4NkJEQjA0QkU4MjAwQjkxM0YwMjkzNEJFMDBCQkNBQTQ5RTczIiwidXBuIjoiYWd1bGFvQG9ucy5vcmcuYnIiLCJpc3MiOiJodHRwOi8vcG9wZHN2Lm9ucy5vcmcuYnIvb25zLnBvcC5mZWRlcmF0aW9uLyIsImF1ZCI6Ik1vYmlsZS5NZXVPTlMiLCJleHAiOjE1MzU0NzM0OTgsIm5iZiI6MTUzNTQ3MzE5OH0.pF73BcZLAbHTGCImCIWVHHQ2O93jlov_5VDsAwKTqFY")
    console.log('token do package', this.tokenSrv.getToken());

    console.log(jwt(this.tokenTest.access_token))

      this.storageSrv.Gravar("jwtTokenName",this.tokenSrv.getToken());
  console.log('Token:',this.tokenSrv.getToken());

  }
  start() {
    this.securitySrv.init();
    
    /*this.tokenSrv.setToken(this.tokenTest)
    console.log(this.tokenSrv.getClaims());
    this.tokenSrv.getClaims().forEach((element: any) => {
      const X = this.ClaimTags.Group
      if (element.nome === X) {
        element.valor.forEach((g: string) => {
          console.log(g);
        });
      }

    })*/
  }

  goToPermissoes(){
  this.navCtrl.push(PermissoesPage);
  }

  goToPermissoesSemEscopo(){
    this.navCtrl.push(PermissoesnoscopePage);
  }

  sair(){
    this.loginSrv.analyticsSrv.sendCustomEvent('Sair');
    this.loginSrv.logout();
  }
}
