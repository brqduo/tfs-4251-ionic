import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermissoesnoscopePage } from './permissoesnoscope';

@NgModule({
  declarations: [
    PermissoesnoscopePage,
  ],
  imports: [
    IonicPageModule.forChild(PermissoesnoscopePage),
  ],
})
export class PermissoesnoscopePageModule {}
