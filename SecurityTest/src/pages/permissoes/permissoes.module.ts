import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PermissoesPage } from './permissoes';

@NgModule({
  declarations: [
    PermissoesPage,
  ],
  imports: [
    IonicPageModule.forChild(PermissoesPage),
  ],
})
export class PermissoesPageModule {}
