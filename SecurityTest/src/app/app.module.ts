import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';


import {
  OnsPackage,
  LoginService,
  ErrorService,
  LoginPage,
  UserService,
  StorageService,
  ImageService,
  SecurityService,
  NetWorkService
} from '@ons/ons-mobile-login';

import {
  AnalyticsService,
  OnsAnalyticsModule
} from '@ons/ons-mobile-analytics'


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { PermissoesPage } from '../pages/permissoes/permissoes';
import { PermissoesnoscopePage } from '../pages/permissoesnoscope/permissoesnoscope';




import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PermissoesPage,
    PermissoesnoscopePage
  ],
  imports: [
    BrowserModule,
    OnsPackage.forRoot(),
    HttpClientModule,
    OnsAnalyticsModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    PermissoesPage,
    PermissoesnoscopePage 
  ],
  providers: [
    StatusBar,
    AnalyticsService,
    LoginService,
    ErrorService,
    UserService,
    StorageService,
    ImageService,
    SecurityService,
    NetWorkService,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
