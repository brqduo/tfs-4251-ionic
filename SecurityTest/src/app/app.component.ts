import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { LoginService, EnvironmentService, SecurityService } from '@ons/ons-mobile-login';
import { LoginPage, ImageService } from '@ons/ons-mobile-login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any;

  constructor(platform: Platform,
    statusBar: StatusBar,
    public loginSrv: LoginService,
    splashScreen: SplashScreen,
    public imageSrv: ImageService,
    public envSrv:EnvironmentService,
    public securitySrv: SecurityService,
    public app: App ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      imageSrv.SetDefault();
      loginSrv.setAplicationName('Mobile.Contatos')
      envSrv.setEnv('DSV')
      this.rootPage = LoginPage;
     // this.navCtrl.setRoot(LoginPage)

     this.loginSrv.onConectedChange
     .subscribe((u: any) => {
       if (u !== undefined) {
         if (u.Connected) {
           console.log('castle 2', u);
          this.app.getActiveNav().setRoot(HomePage);
         }
       }
     })

    });


  }
}

