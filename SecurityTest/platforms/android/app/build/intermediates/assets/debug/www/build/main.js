webpackJsonp([2],{

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PermissoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PermissoesPage = /** @class */ (function () {
    function PermissoesPage(navCtrl, navParams, securitySrv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.securitySrv = securitySrv;
        this.UseSecurity = true;
        this.ScopeTypeOperationAllowed = false;
        this.ScopeTypeGroupAllowed = false;
        this.ScopeTypeRoleAllowed = false;
        this.IsGlobal = false;
        this.inputScope = "ONS";
        this.inputType = "ONS";
        this.inputOperation = "";
        this.inputGroup = "";
        this.inputRole = "";
    }
    PermissoesPage.prototype.globalChange = function () {
        this.init();
    };
    PermissoesPage.prototype.scopeChange = function () {
        this.init();
    };
    PermissoesPage.prototype.typeChange = function () {
        this.init();
    };
    PermissoesPage.prototype.groupChange = function () {
        // if(!this.IsGlobal)
        // {
        this.ScopeTypeGroupAllowed = this.securitySrv.isGroupAllowedByScope(this.inputScope, this.inputType, this.inputGroup);
        // }
        // else
        // {
        //   this.ScopeTypeGroupAllowed = true;
        // }
    };
    PermissoesPage.prototype.roleChange = function () {
        // if(!this.IsGlobal)
        // {
        this.ScopeTypeRoleAllowed = this.securitySrv.isRoleAllowedByScope(this.inputScope, this.inputType, this.inputRole);
        // }
        // else
        // {
        //   this.ScopeTypeRoleAllowed = true;
        // }
    };
    PermissoesPage.prototype.operationChange = function () {
        if (!this.IsGlobal) {
            this.ScopeTypeOperationAllowed = this.securitySrv.isOperationAllowedByScope(this.inputScope, this.inputType, this.inputOperation);
        }
        else {
            this.ScopeTypeOperationAllowed = this.securitySrv.isGlobalAccessByScope(this.inputScope, this.inputOperation);
            //true;
        }
    };
    PermissoesPage.prototype.init = function () {
        this.groupChange();
        this.roleChange();
        this.operationChange();
    };
    PermissoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-permissoes',template:/*ion-inline-start:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\permissoes\permissoes.html"*/'<!--\n\n  Generated template for the PermissoesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Teste Permissões</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n<ion-grid>\n\n    <ion-row>\n\n      <ion-col>\n\n          <ion-item no-lines>\n\n              <ion-label>Security Service</ion-label>\n\n              <ion-toggle [(ngModel)]="UseSecurity"></ion-toggle>\n\n          </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n          <ion-item no-lines>\n\n              <ion-label>Global Access Operation</ion-label>\n\n              <ion-toggle [(ngModel)]="IsGlobal" (ionChange)="globalChange()"></ion-toggle>\n\n          </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n         <ion-item no-lines>\n\n           <ion-label color="primary">Escopo:</ion-label>\n\n           <ion-input placeholder="Insira o Escopo" [(ngModel)]="inputScope"  (ionChange)="scopeChange()"></ion-input>\n\n         </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n          <ion-item no-lines>\n\n            <ion-label color="primary">Tipo:</ion-label>\n\n            <ion-input placeholder="Insira o Tipo" [(ngModel)]="inputType"  (ionChange)="typeChange()"></ion-input>\n\n          </ion-item>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n         <ion-item no-lines>\n\n           <ion-label color="primary">Grupo:</ion-label>\n\n           <ion-input placeholder="Insira o grupo" [(ngModel)]="inputGroup" (ionChange)="groupChange()"></ion-input>\n\n         </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n          <ion-col>\n\n           <ion-item no-lines>\n\n             <ion-label color="primary">Perfil:</ion-label>\n\n             <ion-input placeholder="Insira o perfil" [(ngModel)]="inputRole" (ionChange)="roleChange()"></ion-input>\n\n           </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n         <ion-item no-lines>\n\n           <ion-label color="primary">Operação:</ion-label>\n\n           <ion-input placeholder="Insira a operação" [(ngModel)]="inputOperation"  (ionChange)="operationChange()"></ion-input>\n\n         </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n  <ion-row>\n\n    <ion-col>\n\n       <button *ngIf="((ScopeTypeOperationAllowed == true && UseSecurity == true) || (UseSecurity == false))" ion-button full>Escopo/Tipo/Operação</button>\n\n    </ion-col>\n\n  </ion-row>\n\n  <ion-row>\n\n      <ion-col>\n\n         <button *ngIf="((ScopeTypeGroupAllowed == true && UseSecurity == true) || (UseSecurity == false))" ion-button full>Escopo/Tipo/Grupo</button>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n           <button *ngIf="((ScopeTypeRoleAllowed == true && UseSecurity == true) || (UseSecurity == false) )" ion-button full>Escopo/Tipo/Perfil</button>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row class="none">\n\n        <ion-col>\n\n           <button ion-button full>Operação/Tipo</button>\n\n        </ion-col>\n\n    </ion-row>\n\n    <ion-row class="none">\n\n        <ion-col>\n\n           <button ion-button full>Perfil/Tipo</button>\n\n        </ion-col>\n\n    </ion-row>\n\n</ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\permissoes\permissoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["SecurityService"]])
    ], PermissoesPage);
    return PermissoesPage;
}());

//# sourceMappingURL=permissoes.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PermissoesnoscopePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PermissoesnoscopePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PermissoesnoscopePage = /** @class */ (function () {
    function PermissoesnoscopePage(navCtrl, navParams, securitySrv) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.securitySrv = securitySrv;
        this.UseSecurity = true;
        this.OperationAllowed = false;
        this.GroupAllowed = false;
        this.RoleAllowed = false;
        this.IsGlobal = false;
        this.inputOperation = "";
        this.inputGroup = "";
        this.inputRole = "";
    }
    PermissoesnoscopePage.prototype.globalChange = function () {
        this.groupChange();
        this.roleChange();
        this.operationChange();
    };
    PermissoesnoscopePage.prototype.groupChange = function () {
        // if(!this.IsGlobal)
        // {
        this.GroupAllowed = this.securitySrv.IsAllowed(__WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["Config"].ClaimTags.Group, this.inputGroup);
        // }
        // else
        // {
        //   this.GroupAllowed = true;
        // }
    };
    PermissoesnoscopePage.prototype.roleChange = function () {
        // if(!this.IsGlobal)
        // {
        this.RoleAllowed = this.securitySrv.isRoleAllowed(this.inputRole);
        // }
        // else
        // {
        //   this.RoleAllowed = true;
        // }
    };
    PermissoesnoscopePage.prototype.operationChange = function () {
        if (!this.IsGlobal) {
            this.OperationAllowed = this.securitySrv.IsAllowed(__WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["Config"].ClaimTags.Operation, this.inputOperation);
        }
        else {
            this.OperationAllowed = this.securitySrv.isGlobalAccess(this.inputOperation); // true;
        }
    };
    PermissoesnoscopePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-permissoesnoscope',template:/*ion-inline-start:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\permissoesnoscope\permissoesnoscope.html"*/'<!--\n\n  Generated template for the PermissoesPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Teste Permissões Sem Escopo</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding>\n\n  <ion-grid>\n\n      <ion-row>\n\n        <ion-col>\n\n            <ion-item no-lines>\n\n                <ion-label>Security Service</ion-label>\n\n                <ion-toggle [(ngModel)]="UseSecurity"></ion-toggle>\n\n            </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n      <!--<ion-row>\n\n        <ion-col>\n\n            <ion-item no-lines>\n\n                <ion-label>Global Access Operation</ion-label>\n\n                <ion-toggle [(ngModel)]="IsGlobal" (ionChange)="globalChange()"></ion-toggle>\n\n            </ion-item>\n\n        </ion-col>\n\n      </ion-row>-->\n\n      <!--<ion-row>\n\n          <ion-col>\n\n           <ion-item no-lines>\n\n             <ion-label color="primary">Escopo:</ion-label>\n\n             <ion-input placeholder="Insira o Escopo" [(ngModel)]="inputScope" disabled></ion-input>\n\n           </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n      <ion-row>\n\n          <ion-col>\n\n            <ion-item no-lines>\n\n              <ion-label color="primary">Tipo:</ion-label>\n\n              <ion-input placeholder="Insira o Tipo" [(ngModel)]="inputType" disabled></ion-input>\n\n            </ion-item>\n\n          </ion-col>\n\n      </ion-row>-->\n\n      <ion-row>\n\n          <ion-col>\n\n           <ion-item no-lines>\n\n             <ion-label color="primary">Grupo:</ion-label>\n\n             <ion-input placeholder="Insira o grupo" [(ngModel)]="inputGroup" (ionChange)="groupChange()"></ion-input>\n\n           </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n        <ion-row>\n\n            <ion-col>\n\n             <ion-item no-lines>\n\n               <ion-label color="primary">Perfil:</ion-label>\n\n               <ion-input placeholder="Insira o perfil" [(ngModel)]="inputRole" (ionChange)="roleChange()"></ion-input>\n\n             </ion-item>\n\n            </ion-col>\n\n          </ion-row>\n\n      <ion-row>\n\n          <ion-col>\n\n           <ion-item no-lines>\n\n             <ion-label color="primary">Operação:</ion-label>\n\n             <ion-input placeholder="Insira a operação" [(ngModel)]="inputOperation"  (ionChange)="operationChange()"></ion-input>\n\n           </ion-item>\n\n          </ion-col>\n\n        </ion-row>\n\n    <ion-row>\n\n      <ion-col>\n\n         <button *ngIf="((OperationAllowed == true && UseSecurity == true) || (UseSecurity == false))" ion-button full>Operação</button>\n\n      </ion-col>\n\n    </ion-row>\n\n    <ion-row>\n\n        <ion-col>\n\n           <button *ngIf="((GroupAllowed == true && UseSecurity == true) || (UseSecurity == false))" ion-button full>Grupo</button>\n\n        </ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n          <ion-col>\n\n             <button *ngIf="((RoleAllowed == true && UseSecurity == true) || (UseSecurity == false) )" ion-button full>Perfil</button>\n\n          </ion-col>\n\n      </ion-row>\n\n      <!--<ion-row class="none">\n\n          <ion-col>\n\n             <button ion-button full>Operação/Tipo</button>\n\n          </ion-col>\n\n      </ion-row>\n\n      <ion-row class="none">\n\n          <ion-col>\n\n             <button ion-button full>Perfil/Tipo</button>\n\n          </ion-col>\n\n      </ion-row>-->\n\n  </ion-grid>\n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\permissoesnoscope\permissoesnoscope.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__ons_ons_mobile_login__["SecurityService"]])
    ], PermissoesnoscopePage);
    return PermissoesnoscopePage;
}());

//# sourceMappingURL=permissoesnoscope.js.map

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 166;

/***/ }),

/***/ 210:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/permissoes/permissoes.module": [
		695,
		1
	],
	"../pages/permissoesnoscope/permissoesnoscope.module": [
		696,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 210;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__permissoes_permissoes__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__permissoesnoscope_permissoesnoscope__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jwt_decode__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jwt_decode___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jwt_decode__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, storageSrv, platform, securitySrv, imageSrv, loginSrv, viewCtrl, tokenSrv) {
        this.navCtrl = navCtrl;
        this.storageSrv = storageSrv;
        this.platform = platform;
        this.securitySrv = securitySrv;
        this.imageSrv = imageSrv;
        this.loginSrv = loginSrv;
        this.viewCtrl = viewCtrl;
        this.tokenSrv = tokenSrv;
        this.tokenTest = {
            access_token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9hdXRob3JpemF0aW9uZGVjaXNpb24iOiJNb2JpbGUuTWV1T05TIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYXV0aGVudGljYXRpb24iOiJQT1AiLCJlbWFpbCI6ImFndWxhb0BvbnMub3JnLmJyIiwidW5pcXVlX25hbWUiOiJvbnNcXGFndWxhbyIsIm5hbWVpZCI6Im9uc1xcYWd1bGFvIiwid2luYWNjb3VudG5hbWUiOiJvbnNcXGFndWxhbyIsImdpdmVuX25hbWUiOiJBTkRFUlNPTiBSQU5HRUwgR1VMQU8iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDE1LzA3L2lkZW50aXR5L2NsYWltcy9zY29wZW9wZXJhdGlvbiI6WyJPTlMvT05TfCMkJUFwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlTGlzdGFyIFJlY2ViaW1lbnRvcyBGw61zaWNvcyIsIk9OUy9PTlN8IyQlUmVwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIGFycXVpdm8gYW5leG8gcmVjZWJpbWVudG8gZsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIHJlZ2lzdHJvcyBkYSBjYXRyYWNhIl0sImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3Njb3Blcm9sZSI6WyJPTlMvT05TfCMkJUFwcm92YWRvciBUw6ljbmljbyIsIk9OUy9PTlN8IyQlRnVuY2lvbsOhcmlvIEdlcmFsIiwiT05TL09OU3wjJCVHZXN0b3IiXSwicm9sZSI6WyJBcHJvdmFkb3IgVMOpY25pY28iLCJGdW5jaW9uw6FyaW8gR2VyYWwiLCJHZXN0b3IiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiUy0xLTUtMjEtNjQ0Mjk4OTg1LTE5NjI5Mjg2MC0xOTE2ODE1ODM2LTcxNzQwIiwicHJpbWFyeXNpZCI6IlMtMS01LTIxLTY0NDI5ODk4NS0xOTYyOTI4NjAtMTkxNjgxNTgzNi03MTc0MCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3VzZXJ0aWNrZXQiOiJCMTVCMjc5NkM4MzFCNkFBN0EwRTEzNDMyNjE1N0Q2REIwRDY1NDNGOTY2NDNEMjgxNjgwRkQ5MjBFQzU4NzQ4NENDMzZFNjk1NUE5OThCNTA4MTVDQjU0NjJDMzFEMTdCMDc0RUI1NjE1QUZCM0E0NDJFQkRGOTlFMkFCMTJDMkIyQjg4MjNDOUJGQzQ1Mzc2QTJFQTlFOUMwNUZGRTkzNEM3QURDQjA5M0I1QjcxNzQxMUM4QkM4RTFCOUExM0E4NzQ4NkJEQjA0QkU4MjAwQjkxM0YwMjkzNEJFMDBCQkNBQTQ5RTczIiwidXBuIjoiYWd1bGFvQG9ucy5vcmcuYnIiLCJpc3MiOiJodHRwOi8vcG9wZHN2Lm9ucy5vcmcuYnIvb25zLnBvcC5mZWRlcmF0aW9uLyIsImF1ZCI6Ik1vYmlsZS5NZXVPTlMiLCJleHAiOjE1MzU0NzM0OTgsIm5iZiI6MTUzNTQ3MzE5OH0.pF73BcZLAbHTGCImCIWVHHQ2O93jlov_5VDsAwKTqFY",
            expires_in: 1199,
            refresh_token: "fY9gNU9s8eXre/DeajBRyGCBKNgNzfPj4YRqlyq7QEs=",
            token_type: "bearer",
        };
        this.ClaimTags = {
            Group: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/group',
            ScopeRole: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/scoperole',
            UserTicket: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/userticket',
            Scope: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/scope',
            Operation: 'http://schemas.xmlsoap.org/ws/2015/07/identity/claims/operation'
        };
        loginSrv.setAplicationName('Mobile.Contatos');
        console.log('Home Page');
        var groups = this.securitySrv.Groups;
        console.log('Grupos: ');
        console.log(groups);
        //this.getDecode();
        console.log('Token: ', this.tokenSrv.getToken());
        platform.ready().then(function () {
            if (!loginSrv.CurrentUser.Connected) {
                imageSrv.SetDefault();
                //this.navCtrl.setRoot(HomePage)
                //navCtrl.setRoot(LoginPage)
            }
        });
        //this.start();
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loginSrv.onConectedChange
            .subscribe(function (u) {
            if (u !== undefined) {
                if (u.Connected) {
                    console.log('castle 2');
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["LoginPage"]);
                }
            }
        });
    };
    HomePage.prototype.getDecode = function () {
        this.tokenSrv.setToken(this.tokenTest);
        // this.tokenSrv.setToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9hdXRob3JpemF0aW9uZGVjaXNpb24iOiJNb2JpbGUuTWV1T05TIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvYXV0aGVudGljYXRpb24iOiJQT1AiLCJlbWFpbCI6ImFndWxhb0BvbnMub3JnLmJyIiwidW5pcXVlX25hbWUiOiJvbnNcXGFndWxhbyIsIm5hbWVpZCI6Im9uc1xcYWd1bGFvIiwid2luYWNjb3VudG5hbWUiOiJvbnNcXGFndWxhbyIsImdpdmVuX25hbWUiOiJBTkRFUlNPTiBSQU5HRUwgR1VMQU8iLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDE1LzA3L2lkZW50aXR5L2NsYWltcy9zY29wZW9wZXJhdGlvbiI6WyJPTlMvT05TfCMkJUFwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlTGlzdGFyIFJlY2ViaW1lbnRvcyBGw61zaWNvcyIsIk9OUy9PTlN8IyQlUmVwcm92YXIgUmVjZWJpbWVudG8gRsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIGFycXVpdm8gYW5leG8gcmVjZWJpbWVudG8gZsOtc2ljbyIsIk9OUy9PTlN8IyQlVmVyIHJlZ2lzdHJvcyBkYSBjYXRyYWNhIl0sImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3Njb3Blcm9sZSI6WyJPTlMvT05TfCMkJUFwcm92YWRvciBUw6ljbmljbyIsIk9OUy9PTlN8IyQlRnVuY2lvbsOhcmlvIEdlcmFsIiwiT05TL09OU3wjJCVHZXN0b3IiXSwicm9sZSI6WyJBcHJvdmFkb3IgVMOpY25pY28iLCJGdW5jaW9uw6FyaW8gR2VyYWwiLCJHZXN0b3IiXSwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvc2lkIjoiUy0xLTUtMjEtNjQ0Mjk4OTg1LTE5NjI5Mjg2MC0xOTE2ODE1ODM2LTcxNzQwIiwicHJpbWFyeXNpZCI6IlMtMS01LTIxLTY0NDI5ODk4NS0xOTYyOTI4NjAtMTkxNjgxNTgzNi03MTc0MCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMTUvMDcvaWRlbnRpdHkvY2xhaW1zL3VzZXJ0aWNrZXQiOiJCMTVCMjc5NkM4MzFCNkFBN0EwRTEzNDMyNjE1N0Q2REIwRDY1NDNGOTY2NDNEMjgxNjgwRkQ5MjBFQzU4NzQ4NENDMzZFNjk1NUE5OThCNTA4MTVDQjU0NjJDMzFEMTdCMDc0RUI1NjE1QUZCM0E0NDJFQkRGOTlFMkFCMTJDMkIyQjg4MjNDOUJGQzQ1Mzc2QTJFQTlFOUMwNUZGRTkzNEM3QURDQjA5M0I1QjcxNzQxMUM4QkM4RTFCOUExM0E4NzQ4NkJEQjA0QkU4MjAwQjkxM0YwMjkzNEJFMDBCQkNBQTQ5RTczIiwidXBuIjoiYWd1bGFvQG9ucy5vcmcuYnIiLCJpc3MiOiJodHRwOi8vcG9wZHN2Lm9ucy5vcmcuYnIvb25zLnBvcC5mZWRlcmF0aW9uLyIsImF1ZCI6Ik1vYmlsZS5NZXVPTlMiLCJleHAiOjE1MzU0NzM0OTgsIm5iZiI6MTUzNTQ3MzE5OH0.pF73BcZLAbHTGCImCIWVHHQ2O93jlov_5VDsAwKTqFY")
        console.log('token do package', this.tokenSrv.getToken());
        console.log(__WEBPACK_IMPORTED_MODULE_5_jwt_decode___default()(this.tokenTest.access_token));
        this.storageSrv.Gravar("jwtTokenName", this.tokenSrv.getToken());
        console.log('Token:', this.tokenSrv.getToken());
    };
    HomePage.prototype.start = function () {
        this.securitySrv.init();
        /*this.tokenSrv.setToken(this.tokenTest)
        console.log(this.tokenSrv.getClaims());
        this.tokenSrv.getClaims().forEach((element: any) => {
          const X = this.ClaimTags.Group
          if (element.nome === X) {
            element.valor.forEach((g: string) => {
              console.log(g);
            });
          }
    
        })*/
    };
    HomePage.prototype.goToPermissoes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__permissoes_permissoes__["a" /* PermissoesPage */]);
    };
    HomePage.prototype.goToPermissoesSemEscopo = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__permissoesnoscope_permissoesnoscope__["a" /* PermissoesnoscopePage */]);
    };
    HomePage.prototype.sair = function () {
        this.loginSrv.logout();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\home\home.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title text-center>\n\n     Teste de Security\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content text-center>\n\n <ion-grid>\n\n   <ion-row>\n\n     <ion-col text-center>\n\n        Esta tela é apenas para Testes\n\n     </ion-col>\n\n   </ion-row>\n\n   <ion-row>\n\n     <ion-col>\n\n        <button ion-button full (click)="start()">Iniciar teste do Security</button>\n\n     </ion-col>\n\n   </ion-row>\n\n  \n\n   <!--<ion-row>\n\n     <ion-col>\n\n        <button ion-button full>Exibir Obj</button>\n\n     </ion-col>\n\n     <ion-col>\n\n        <button ion-button full>Permissão do Obj</button>\n\n     </ion-col>\n\n   </ion-row>-->\n\n   <ion-row>\n\n     <ion-col>\n\n        <button (click)="goToPermissoesSemEscopo()" ion-button full>Teste de Permissões</button>\n\n     </ion-col>\n\n   </ion-row>\n\n   <!--<ion-row>\n\n     <ion-col>\n\n        <button ion-button full>Permissão por Grupo</button>\n\n     </ion-col>\n\n     <ion-col>\n\n        <button ion-button full>Negação por Grupo</button>\n\n     </ion-col>\n\n   </ion-row>-->\n\n   <ion-row>\n\n      <ion-col>\n\n         <button (click)="goToPermissoes()" ion-button full>Teste de Permissões Por Escopo</button>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row>\n\n      <ion-col>\n\n         <button (click)="sair()" ion-button full>Sair</button>\n\n      </ion-col>\n\n    </ion-row>\n\n </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\brq\tfs-4251-ionic\SecurityTest\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["StorageService"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["SecurityService"],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["ImageService"],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["LoginService"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ons_ons_mobile_login__["TokenService"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(360);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_analytics__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_analytics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_analytics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(694);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_permissoes_permissoes__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_permissoesnoscope_permissoesnoscope__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_permissoes_permissoes__["a" /* PermissoesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_permissoesnoscope_permissoesnoscope__["a" /* PermissoesnoscopePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["OnsPackage"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["HttpClientModule"],
                __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_analytics__["OnsAnalyticsModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/permissoes/permissoes.module#PermissoesPageModule', name: 'PermissoesPage', segment: 'permissoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/permissoesnoscope/permissoesnoscope.module#PermissoesnoscopePageModule', name: 'PermissoesnoscopePage', segment: 'permissoesnoscope', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["LoginPage"],
                __WEBPACK_IMPORTED_MODULE_9__pages_permissoes_permissoes__["a" /* PermissoesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_permissoesnoscope_permissoesnoscope__["a" /* PermissoesnoscopePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ons_ons_mobile_analytics__["AnalyticsService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["LoginService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["ErrorService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["UserService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["StorageService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["ImageService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["SecurityService"],
                __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["NetWorkService"],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["IonicErrorHandler"] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, loginSrv, splashScreen, imageSrv, envSrv, securitySrv, app) {
        var _this = this;
        this.loginSrv = loginSrv;
        this.imageSrv = imageSrv;
        this.envSrv = envSrv;
        this.securitySrv = securitySrv;
        this.app = app;
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            imageSrv.SetDefault();
            loginSrv.setAplicationName('Mobile.Contatos');
            envSrv.setEnv('DSV');
            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["LoginPage"];
            // this.navCtrl.setRoot(LoginPage)
            _this.loginSrv.onConectedChange
                .subscribe(function (u) {
                if (u !== undefined) {
                    if (u.Connected) {
                        console.log('castle 2', u);
                        _this.app.getActiveNav().setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    }
                }
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"C:\brq\tfs-4251-ionic\SecurityTest\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\brq\tfs-4251-ionic\SecurityTest\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["LoginService"],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["ImageService"],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["EnvironmentService"],
            __WEBPACK_IMPORTED_MODULE_5__ons_ons_mobile_login__["SecurityService"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["App"]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[355]);
//# sourceMappingURL=main.js.map