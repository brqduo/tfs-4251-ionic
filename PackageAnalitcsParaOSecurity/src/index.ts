import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsService } from './services/analytics.service';

export * from './services/analytics.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: []
})
export class OnsAnalyticsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: OnsAnalyticsModule,
      providers: [AnalyticsService]
    };
  }
}
