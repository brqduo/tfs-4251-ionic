"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
window.fabric = window.fabric || {};
var AnalyticsService = (function () {
    function AnalyticsService(platform) {
        this.platform = platform;
    }
    AnalyticsService.prototype.sendCustomEvent = function (eventName, customAttributes) {
        var _this = this;
        if (customAttributes === void 0) { customAttributes = {}; }
        return new Promise(function (resolve, reject) {
            _this.platform.ready()
                .then(function () {
                if (_this.platform.is('cordova')) {
                    window.fabric.Answers.sendCustomEvent(eventName, customAttributes);
                }
                else {
                }
                resolve(null);
            })["catch"](function (error) {
                window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
                window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
                reject(error);
            });
        });
    };
    AnalyticsService.prototype.sendContentView = function (contentName, contentType, contentId, customAttributes) {
        var _this = this;
        if (customAttributes === void 0) { customAttributes = {}; }
        return new Promise(function (resolve, reject) {
            _this.platform.ready()
                .then(function () {
                if (_this.platform.is('cordova')) {
                    window.fabric.Answers.sendContentView(contentName, contentType, contentId, customAttributes);
                }
                else {
                    // console.log(contentName + ' not viewed. no cordova (device/simulator) detected.');
                }
                resolve(null);
            })["catch"](function (error) {
                window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
                window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
                reject(error);
            });
        });
    };
    AnalyticsService.prototype.sendNonFatalCrash = function (msg, err) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.error('sendNonFatalCrash will be notified');
            _this.platform.ready()
                .then(function () {
                if (_this.platform.is('cordova')) {
                    window.fabric.Crashlytics.addLog(msg + ': ' + JSON.stringify(err));
                    window.fabric.Crashlytics.sendNonFatalCrash(msg + ': ' + JSON.stringify(err));
                    console.error(msg + ': ' + JSON.stringify(err));
                }
                else {
                    console.error(msg + ': ' + JSON.stringify(err) + ' SendNonFatalCrash not notified. no cordova (device/simulator) detected.');
                }
                resolve(null);
            })["catch"](function (error) {
                window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
                window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
                reject(error);
            });
        });
    };
    AnalyticsService.prototype.sendCrash = function (msg, err) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.error('sendCrash will be notified');
            _this.platform.ready()
                .then(function () {
                if (_this.platform.is('cordova')) {
                    window.fabric.Crashlytics.addLog(msg + ': ' + JSON.stringify(err));
                    window.fabric.Crashlytics.sendCrash(msg + ': ' + JSON.stringify(err));
                    console.error(msg + ': ' + JSON.stringify(err));
                }
                else {
                    console.error(msg + ': ' + JSON.stringify(err) + ' sendCrash not notified. no cordova (device/simulator) detected.');
                }
                resolve(null);
            })["catch"](function (error) {
                window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
                window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
                reject(error);
            });
        });
    };
    AnalyticsService = __decorate([
        core_1.Injectable()
    ], AnalyticsService);
    return AnalyticsService;
}());
exports.AnalyticsService = AnalyticsService;
