import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Storage } from "@ionic/storage";

declare global {
  interface Window { fabric: any; }
}
window.fabric = window.fabric || {};

@Injectable()
export class AnalyticsService {

  constructor(private platform: Platform) {
  }

  public sendCustomEvent(eventName: string, customAttributes = {}): Promise<any> {
    console.log('send custon event');

    return new Promise((resolve, reject) => {
      this.platform.ready()
        .then(() => {
          if (this.platform.is('cordova')) {
            // console.log('sending...', window.fabric);
            window.fabric.Answers.sendCustomEvent(eventName, customAttributes);
          } else {
            console.log('platform is not cordova');

          }
          resolve(null);
        })
        .catch((error) => {
          //  window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
          window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
          reject(error);
        });
    });
  }

  public sendContentView(contentName: string, contentType: string, contentId: string, customAttributes = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      this.platform.ready()
        .then(() => {
          if (this.platform.is('cordova')) {
            window.fabric.Answers.sendContentView(contentName, contentType, contentId, customAttributes);
          } else {
            // console.log(contentName + ' not viewed. no cordova (device/simulator) detected.');
          }
          resolve(null);
        })
        .catch((error) => {
          window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
          window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
          reject(error);
        });
    });
  }

  public sendNonFatalCrash(msg: string, err: any): Promise<any> {
    return new Promise((resolve, reject) => {
      console.error('sendNonFatalCrash will be notified', window);
      this.platform.ready()
        .then(() => {
          if (this.platform.is('cordova')) {
            //       window.fabric.Crashlytics.addLog(msg + ': ' + JSON.stringify(err));
            window.fabric.Crashlytics.sendNonFatalCrash(msg + ': ' + JSON.stringify(err));
            console.error(msg + ': ' + JSON.stringify(err));
          } else {
            console.error(msg + ': ' + JSON.stringify(err) + ' SendNonFatalCrash not notified. no cordova (device/simulator) detected.');
          }
          resolve(null);
        })
        .catch((error) => {
          window.fabric.Crashlytics.addLog('Plataform not ready: ' + JSON.stringify(error));
          window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
          reject(error);
        });
    });
  }

  public sendCrash(msg: string, err: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.platform.ready()
        .then(() => {
          if (this.platform.is('cordova')) {
            window.fabric.Crashlytics.addLog(msg + ': ' + JSON.stringify(err));
            window.fabric.Crashlytics.sendNonFatalCrash(msg, err)
          } else {
            console.error(msg + ': ' + JSON.stringify(err) + ' sendCrash not notified. no cordova (device/simulator) detected.');
          }
          resolve(null);
        })
        .catch((error) => {
          window.fabric.Crashlytics.sendNonFatalCrash('Plataform not ready: ' + JSON.stringify(error));
          reject(error);
        });
    });
  }

}
